
(load "SDE-scheme-utilities.scm")
(load "sdeoffsetmask.scm")


(sde:clear)

(define GDSFILE "TCAD_PIXEL_v3.gds")
(define CELLNAME "TCAD_PIXEL_v3")
(define LAYERNAMES (list 'PWELL 'POLY 'ACT 'NO_PW 'NPLUS 'CONT 'PW_LVT 'MET1 'VIA1 'MET2 'VIA2 'MET3 'VIA3 'MET4 'ULENS 'PD1 'PD2 'SN1 'SN2 'SN3 ))
(define LAYERNUMBERS (list '1:0 '8:0 '9:0 '17:82 '32:0 '34:0 '35:0 '40:0 '41:0 '42:0 '43:0 '44:0 '49:0 '50:0 '89:0 '92:82 '93:0 '94:0 '94:43 '94:95 ))

(sdeicwb:gds2mac "gds.file" GDSFILE "cell" CELLNAME "layer.names" LAYERNAMES "layer.numbers" LAYERNUMBERS "sim3d" (list 0 -6000 6000 0) "scale" 1.0e-3 "domain.name" "SIM3D" "mac.file" "TCAD_PIXEL")

(read-layout-init-domain "TCAD_PIXEL_SIM3D.mac" #f "SIM3D")


(define TSUB 7.0)

(sdepe:add-substrate "material" "Silicon" "thickness" TSUB "region" "substrat") 

(sdepe:pattern "mask" "ACT" "polarity" "light" "material" "Resist" "thickness" 1  "type" "aniso" "algorithm" "sweep" )

(sdepe:etch-material "material" "Silicon" "depth" 0.420  "taper-angle" 5) 

(entity:delete (find-material-id "Resist"))

(sdepe:fill-device "material" "Oxide" "height" (+ TSUB 0.008))


(sdepe:pattern "mask" "POLY" "polarity" "light" "material" "PolySilicon" "thickness" 0.3  "type" "aniso" "algorithm" "sweep" )

(sdepe:depo "material" "dummy" "thickness" 0.03 "type" "iso" "algorithm" "lopx")
(sdepe:etch-material "material" "dummy" "depth" 0.03  "type" "aniso") 



(sdepe:depo "material" "Nitride" "thickness" 0.120 "type" "iso" "algorithm" "lopx" )
;;;(sdepe:etch-material "material" "Nitride" "depth" 0.160  "type" "aniso"  "radius" 0.120 "vexity" "concave" "algorithm" "lopx" "ebl" "top") 
(sdepe:etch-material "material" "Nitride" "depth" 0.160  "type" "aniso")
(fillet-selected-external-edges-at-z "Nitride" (+ TSUB 0.008 (- 0.3 0.04)) 0.12)



(sdepe:fill-device "material" "FSG" "height" (+ TSUB 0.7))
(sdepe:pattern "mask" "CONT" "polarity" "dark" "material" "Resist" "thickness" 1.0  "type" "aniso" "algorithm" "sweep" )
(sdepe:etch-material "material" "FSG" "depth" 2.0  "type" "aniso") 
(sdepe:etch-material "material" "dummy" "depth" 0.1  "type" "aniso") 
(sdepe:etch-material "material" "Oxide" "depth" 0.1  "type" "aniso") 
(entity:delete (find-material-id "Resist"))
(sdepe:fill-device "material" "Aluminum" )

(sdepe:icon_layer "mask" "MET1" "thickness" 0.4 "polarity" "light" "ic-material" "Aluminum" "env-material" "FSG" "taper-angle" 0)
(sdepe:icon_layer "mask" "VIA1" "thickness" 0.6 "polarity" "light" "ic-material" "Aluminum" "env-material" "FSG" "taper-angle" 0)
(sdepe:icon_layer "mask" "MET2" "thickness" 0.4 "polarity" "light" "ic-material" "Aluminum" "env-material" "FSG" "taper-angle" 0)

(define bbox-global (bbox (get-body-list)))
	(define ZGmax (position:z (cdr bbox-global)))



(sdepe:icon_layer "mask" "VIA2" "thickness" 0.6 "polarity" "light" "ic-material" "Aluminum" "env-material" "FSG" "taper-angle" 0)
(sdepe:icon_layer "mask" "MET3" "thickness" 0.4 "polarity" "light" "ic-material" "Aluminum" "env-material" "FSG" "taper-angle" 0)

(define bbox-global (bbox (get-body-list)))
	(define ZGmax (position:z (cdr bbox-global)))


(sdepe:icon_layer "mask" "VIA3" "thickness" 0.9 "polarity" "light" "ic-material" "Aluminum" "env-material" "FSG" "taper-angle" 0)
(sdepe:icon_layer "mask" "MET4" "thickness" 0.6 "polarity" "light" "ic-material" "Aluminum" "env-material" "FSG" "taper-angle" 0)

(define bbox-global (bbox (get-body-list)))
	(define XGmax (position:x (cdr bbox-global)))
	(define XGmin (position:x (car bbox-global)))
	(define YGmax (position:y (cdr bbox-global)))
	(define YGmin (position:y (car bbox-global)))
	(define ZGmax (position:z (cdr bbox-global)))
	(define ZGmin (position:z (car bbox-global)))
	

(sdepe:depo "material" "FSG" "thickness" 0.3 "type" "aniso" )


(bool:unite (find-material-id "FSG"))
(bool:unite (find-material-id "Aluminum"))

(rename-region "Aluminum" "Aluminum" "Alu")
(rename-region "FSG" "FSG" "FSG")
(rename-region "dummy" "Oxide" "REOX")
(rename-region "PolySilicon" "PolySilicon" "PolyGate")
(rename-region "Nitride" "Nitride" "NiSpacer")



(part:save "n@node@_model.sat")
(sdeio:save-tdr-bnd (get-body-list) "n@node@_model_bnd.tdr")


	(define bbox-ULENS (bbox (find-mask "ULENS")))

	(define xmaxULENS (position:x (cdr bbox-ULENS)))
	(define xminULENS (position:x (car bbox-ULENS)))
	(define ymaxULENS (position:y (cdr bbox-ULENS)))
	(define yminULENS (position:y (car bbox-ULENS)))
	
	(define xmidULENS (* 0.5 (+ xmaxULENS xminULENS)))
	(define ymidULENS (* 0.5 (+ ymaxULENS yminULENS)))
	
(define bbox-global (bbox (get-body-list)))
	(define XGmax (position:x (cdr bbox-global)))
	(define XGmin (position:x (car bbox-global)))
	(define YGmax (position:y (cdr bbox-global)))
	(define YGmin (position:y (car bbox-global)))
	(define ZGmax (position:z (cdr bbox-global)))
	(define ZGmin (position:z (car bbox-global)))
	

(sde:clear)

(refinement:set-prop sde-ref "grid mode"  "AF_GRID_ONE_DIR")
(sde:setrefprops 0 5 0 0)
(render:rebuild)

(sdegeo:set-default-boolean "BAB")

(define Lradius 4.561)
(define zULENS  (- ZGmax (- Lradius 1.0)))
(sdegeo:create-cuboid (position (- XGmin 10) (- YGmin 10) ZGmax)  (position (+ XGmax 10) (+ YGmax 10) ( - ZGmin (* 3.0 Lradius))) "tmp" "gtmp")

(sdegeo:create-sphere (position xmidULENS ymidULENS zULENS) Lradius "ULENS" "R_ULENS")

(sdegeo:chop-domain (list  XGmin YGmax    XGmax YGmax    XGmax YGmin    XGmin YGmin   ))
(entity:delete (find-material-id "tmp"))

(sdegeo:create-cuboid (position XGmin YGmin ZGmax)  (position XGmax YGmax ( + ZGmax 2.0) ) "Vacuum" "RTopGas")

(part:save "n@node@_lens.sat")
(sdeio:save-tdr-bnd (get-body-list) "n@node@_lens_bnd.tdr")



(sde:clear)


(sdeio:read-tdr-bnd "n@node@_lens_bnd.tdr")
(sdeio:read-tdr-bnd "n@node@_model_bnd.tdr")


(sdeio:save-tdr-bnd (get-body-list) "n@node@_bnd.tdr")
	


