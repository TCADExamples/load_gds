
pdbSet Mechanics EtchDepoRelax 0

icwb gds.file= TCAD_PIXEL_v3.gds cell= TCAD_PIXEL_v3 \
layer.numbers= "1:0 8:0 9:0 17:82 32:0 34:0 35:0 40:0 41:0 42:0 43:0 44:0 49:0 50:0 89:0 92:82 93:0 94:0 94:43 94:95" \
layer.names= "PWELL POLY ACT NO_PW NPLUS CONT PW_LVT MET1 VIA1 MET2 VIA2 MET3 VIA3 MET4 ULENS PD1 PD2 SN1 SN2 SN3" \
sim3d = "0 0 6000 -6000" domain.name=SIM3D1 \
scale=1e-3



icwb domain= "SIM3D1"

icwb.create.all.masks

fset DIM [icwb dimension]

mask list 
math coord.dfise

fset yleft [icwb bbox left ]
fset yright [icwb bbox right]


fset zfront [icwb bbox front ]
fset zback [icwb bbox back]



line x loc= 0  	  tag=top		    
line x loc= 20 	  tag=bottom  		

line y location= $yleft    tag=left
line y location= $yright    tag=right

line z location= $zback    tag=back
line z location= $zfront    tag=front


region silicon xlo=top xhi=bottom ylo=left yhi=right zlo=back zhi=front
init concentration=1e13 field=Boron 



photo mask=ACT_n thickness=2.0
etch Silicon type=trapezoidal   thickness=420<nm> angle=85 
strip Photoresist

implant boron energy=100 dose=1e15
##etch Silicon  thickness=420<nm> 

struct tdr=n@node@



