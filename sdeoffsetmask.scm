(define sde:offset-mask ; SCM_EXT
  (lambda (mname moffset . options)

    (roll:mark-start)
    (sde:delay-graphics-update #t)
    (define lmaskob #f)

    (define new-mname mname)
    (if (not (null? options))
	(set! new-mname (list-ref options 0)))

    (if (equal? (exists-mask-name mname) #t)
	(begin
	  (define mergeoption (option:get "merge"))
	  (option:set "merge" #t)
	  
	  (define lmask (find-mask mname))
	  (define lmasks (body:separate lmask))
	  (for-each
	   (lambda (lbody)
	     (lop:extend-sheet (entity:edges lbody) moffset)
	     )
	   lmasks)
	  (define lmasksn (list))
	  (for-each
	   (lambda (lbody)
	     (if (solid? lbody)
		 (set! lmasksn (append lmasksn (list lbody))))
	     )
	   lmasks)
	  (set! lmasks lmasksn)
	  (if (> (length lmasks) 1)
	      (begin
		(for-each
		 (lambda (lbody)
		   (bool:unite (car lmasks) lbody)
		   )
		 (cdr lmasks))    
		)
	      )
	  (set! lmaskob (car lmasks))

	  (if (> (length (entity:faces lmaskob)) 1)
	      (begin
		(define umask)
		(for-each
		 (lambda (lface) 
		   (define lfacecopy (entity:copy-contents lface))
		   (define lfsb (sheet:2d (sheet:face lfacecopy)))
		   
		   (if (not (body? umask))
		       (set! umask lfsb)
		       (bool:unite umask lfsb))
		   )
		 (entity:faces lmaskob))
		(entity:delete lmaskob)
		(set! lmaskob umask)
		)
	      )

	  (if (solid? lmaskob)
	      (begin
		(sdegeo:prune-vertices-int lmaskob 0)
		(generic:add lmaskob "maskname" new-mname)
		)
	      )

	  (option:set "merge" mergeoption)
	  )
	)

    (sde:delay-graphics-update #f)
    (roll:mark-end)

    lmaskob
    )
  )
